module Foz.Warp
  ( runFozEnv
  , module Network.Wai.Handler.Warp
  ) where

import Foz
import Network.Wai.Handler.Warp

runFozEnv :: FozApp env -> env -> IO ()
runFozEnv app env =
  withFozApp env app $ runEnv 20000
