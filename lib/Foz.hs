module Foz
  ( Foz
  , FozApp
  , withFozApp

  , FozEnv
  , requestL
  , respondL
  , envL

  , methodL
  , pathL
  , headerL
  , queryL
  , queryParamL
  , requestUri
  , requestUriBase
  , requestUriBS
  , requestUriBaseBS
  , logRequest

  , setStatus
  , setHeader
  , respond
  , respondJson
  , respond404
  , respond500

  , module RIO
  ) where

import RIO
import RIO.State

import qualified Data.Aeson as Aeson
import qualified RIO.State as State
import qualified RIO.Text as Text
import qualified RIO.ByteString.Lazy as LBS
import qualified Network.HTTP.Types as HTTP
import qualified Network.Wai as WAI

data FozEnv env = FozEnv
  { _fozRequest :: WAI.Request
  , _fozRespond :: Respond
  , _fozLogFunc :: LogFunc

  , _fozEnv :: env

  , _fozResponse :: SomeRef FozResponse
  }

instance HasLogFunc (FozEnv env) where
  logFuncL = lens _fozLogFunc $ \fe lf -> fe{_fozLogFunc = lf}

instance HasRequest (FozEnv env) where
  requestL = lens _fozRequest $ \fe req ->fe{_fozRequest = req}

instance HasRespond (FozEnv env) where
  respondL = lens _fozRespond $ \fe res ->fe{_fozRespond = res}

data FozResponse = FozResponse
  { frStatus :: Maybe HTTP.Status
  , frHeaders :: HTTP.ResponseHeaders
  }

instance HasStateRef FozResponse (FozEnv env) where
  stateRefL = lens _fozResponse $ \fe hs -> fe{_fozResponse = hs}

envL :: Lens' (FozEnv env) env
envL = lens _fozEnv (\fe env ->fe{_fozEnv = env})

type Foz env a = RIO (FozEnv env) a

type FozApp env = Foz env WAI.ResponseReceived

withFozApp
  :: (MonadIO m, MonadUnliftIO m)
  => env
  -> FozApp env
  -> (WAI.Application -> m a)
  -> m a
withFozApp env app proc =
  logOptionsHandle stderr True >>= \logOptions ->
    withLogFunc logOptions $ \lf ->
      proc $ \waiRequest waiRespond -> do
        response <- newSomeRef $ FozResponse
          { frStatus = Nothing
          , frHeaders = mempty
          }
        let
          fe = FozEnv
            { _fozRequest = waiRequest
            , _fozRespond = waiRespond
            , _fozLogFunc = lf
            , _fozEnv = env
            , _fozResponse = response
            }

        runRIO fe $ tryAny app >>= \case
          Left (SomeException e) -> do
            logError $ displayShow e
            respond500 "Internal server error."
          Right res -> do
            logDebug "OK"
            pure res

-- * Request utilities

class HasRequest env where
  requestL :: Lens' env WAI.Request

instance HasRequest WAI.Request where
  requestL = id

methodL :: HasRequest env => SimpleGetter env HTTP.Method
methodL = requestL . to WAI.requestMethod

pathL :: HasRequest env => SimpleGetter env [Text]
pathL = requestL . to WAI.pathInfo

headerL :: HasRequest env => HTTP.HeaderName -> SimpleGetter env (Maybe ByteString)
headerL key = requestL . to WAI.requestHeaders . to (lookup key)

queryL :: HasRequest env => SimpleGetter env [(ByteString, Maybe ByteString)]
queryL = requestL . to WAI.queryString

queryParamL :: HasRequest env => ByteString -> SimpleGetter env (Maybe ByteString)
queryParamL key = queryL . to (join . lookup key)

requestUri :: (HasRequest env, MonadReader env m) => m Text
requestUri =
  fmap Text.decodeUtf8' requestUriBS >>= \case
  Left _unicodeError ->
    fail "Unicode error in request path and/or args"
  Right t ->
    pure t

requestUriBS :: (HasRequest env, MonadReader env m) => m ByteString
requestUriBS = do
  req <- view requestL
  base <- requestUriBaseBS
  let
    rawPath = WAI.rawPathInfo req
    query = WAI.rawQueryString req

  pure $ mconcat
    [ base
    , rawPath
    , query
    ]

requestUriBase :: (HasRequest env, MonadReader env m) => m Text
requestUriBase =
  fmap Text.decodeUtf8' requestUriBaseBS >>= \case
  Left _unicodeError ->
    fail "Unicode error in Host header"
  Right t ->
    pure t

requestUriBaseBS :: (HasRequest env, MonadReader env m) => m ByteString
requestUriBaseBS = do
  req <- view requestL
  let
    secure = WAI.isSecure req
    host = fromMaybe "" $ WAI.requestHeaderHost req

  pure $ mconcat
    [ if secure then "https://" else "http://"
    , host
    ]

logRequest :: (MonadIO m, MonadReader (FozEnv env) m) => m ()
logRequest = do
  m <- view methodL

  uri <- requestUriBS

  logInfo $ mconcat
    [ displayBytesUtf8 m
    ,  " "
    , displayBytesUtf8 uri
    ]

-- * Response utilities

setStatus :: MonadState FozResponse m => HTTP.Status -> m ()
setStatus status =
  modify' $ \fr ->
    fr
      { frStatus = Just status
      }

setHeader :: MonadState FozResponse m => HTTP.HeaderName -> ByteString -> m ()
setHeader header value =
  modify' $ \fr ->
    fr
      { frHeaders =
          (header, value) :
            filter (\(k, _v) -> k /= header) (frHeaders fr)
      }

type Respond = WAI.Response -> IO WAI.ResponseReceived

class HasRespond env where
  respondL :: Lens' env Respond

instance HasRespond Respond where
  respondL = id

class ToResponse a where
  toResponseContentType :: a -> Maybe ByteString
  toResponseLbs :: a -> LByteString

instance ToResponse LByteString where
  toResponseContentType _ = Nothing
  toResponseLbs = id

instance ToResponse Text where
  toResponseContentType _ = Just "text/plain"
  toResponseLbs = LBS.fromStrict . Text.encodeUtf8

instance ToResponse Aeson.Value where
  toResponseContentType _ = Just "application/json"
  toResponseLbs = Aeson.encode

respond
  :: ( ToResponse a
    , MonadIO m
    , MonadReader (FozEnv env) m
    , MonadState FozResponse m
    )
  => a -> m WAI.ResponseReceived
respond res = do
  FozResponse{frStatus, frHeaders} <- State.get

  let
    status = fromMaybe HTTP.status200 frStatus

    headers =
      case lookup HTTP.hContentType frHeaders of
        Just _customContentType ->
          frHeaders
        Nothing ->
          case toResponseContentType res of
            Nothing ->
              frHeaders
            Just ct ->
              (HTTP.hContentType, ct) : frHeaders

  respondProc <- view respondL
  liftIO . respondProc $
    WAI.responseLBS status headers $
      toResponseLbs res

respondJson
  :: ( Aeson.ToJSON a
    , MonadIO m
    , MonadReader (FozEnv env) m
    , MonadState FozResponse m
    )
  => a -> m WAI.ResponseReceived
respondJson = respond . Aeson.toJSON

respond404
  :: ( ToResponse a
    , MonadIO m
    , MonadReader (FozEnv env) m
    , MonadState FozResponse m
    )
  => a -> m WAI.ResponseReceived
respond404 res = do
  setStatus HTTP.status404
  respond res

respond500
  :: ( MonadIO m
    , MonadReader (FozEnv env) m
    )
  => LByteString -> m WAI.ResponseReceived
respond500 lbs = do
  respondProc <- view respondL
  liftIO . respondProc $ WAI.responseLBS HTTP.status500 mempty lbs
